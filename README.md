### Notes
---
The only way in or out of the map is via console commands. Use `changemap lotsagun` to move from a level to the reloading room without losing your inventory or `map lotsagun` to start a new game on that map. The alias `GoShopping` is equivalent to `map lotsagun`. To move out of the map, use `changemap`.