GameInfo
{
	AddEventHandlers = "ReloadingRoomHandler"
}

Map LOTSAGUN "Lots of Guns"
{
	Author = "Accensus"
	Sky1 = "SKYGUN", 0.05
	Music = "D_RLROOM"
	EvenLighting
	NoIntermission
}

Episode LOTSAGUN
{
	Name = "Reloading Room"
}

DoomEdNums
{
	15500 = "RR_Stripper"
	15501 = "RR_Cygnis"
	15502 = "RR_Amogus"
	15510 = "RR_Drink1"
	15511 = "RR_Drink2"
	15512 = "RR_Drink3"
}